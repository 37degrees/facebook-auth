Package.describe({
  name:          '37degrees:facebook-auth',
  version:       '0.0.1',
  // Brief, one-line summary of the package.
  summary:       '',
  // URL to the Git repository containing the source code for this package.
  git:           '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Cordova.depends({
  'cordova-plugin-facebook4': '1.4.0'
});

Package.onUse(function (api) {
  api.versionsFrom('1.2.1');

  api.use([
    'ecmascript',
    'check',
    'http',
    'meteor-base',
    'underscore',
    'accounts-base',
    'facebook@1.2.2',
    'btafel:accounts-facebook-cordova@0.0.3'
  ]);

  api.imply(['check', 'accounts-base', 'facebook']);

  api.addFiles('lib/fb_native_logout.js', 'web.cordova');

  api.addFiles(['lib/fb_client.js', 'lib/styles.css'], 'client');
  api.addFiles('lib/fb_server.js', 'server');
  api.addFiles('lib/fb_shared.js', ['client', 'server']);

  api.export('FBUtils');
});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('37degrees:facebook-auth');
  api.addFiles('facebook-auth-tests.js');
});